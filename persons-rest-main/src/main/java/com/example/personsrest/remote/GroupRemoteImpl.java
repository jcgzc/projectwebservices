package com.example.personsrest.remote;

import com.example.personsrest.domain.Person;
import com.example.personsrest.security.KeyCloakToken;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;


public class GroupRemoteImpl implements GroupRemote {

    WebClient webClient;

    public GroupRemoteImpl() {
        webClient = WebClient.builder()
                .baseUrl("https://groups.edu.sensera.se/api/groups/")
                .build();
    }

    @Override
    public String getNameById(String groupId) {
        String JWT = KeyCloakToken.acquire("https://iam.sensera.se/", "test", "group-api", "user", "djnJnPf7VCQvp3Fc")
                .block().getAccessToken();

        return webClient.get()
                .uri("/" + groupId)
                .header("Authorization", "Bearer " + JWT)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Group.class)
                .block()
                .getName();
    }

    @Override
    public String createGroup(String name) {
        String JWT = KeyCloakToken.acquire("https://iam.sensera.se/", "test", "group-api", "user", "djnJnPf7VCQvp3Fc")
                .block().getAccessToken();

        return webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + JWT)
                .body(BodyInserters.fromValue(new CreateGroup(name)))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Group.class)
                .block()
                .getId();

//                .exchange()
//                .expectStatus().isOk()
//                .expectHeader().contentType(MediaType.APPLICATION_JSON)
//                .returnResult(Group.class)
//                .getResponseBody()
//                .single();
    }

    @Override
    public String removeGroup(String groupId) {
        return null;
    }



    public Group get(String name) {
        return webClient.get()
                .uri("/api/groups/" + name)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }


    @Value
    static class Group {
        String id;
        String name;

        @JsonCreator
        public Group(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name) {
            this.id = id;
            this.name = name;
        }
    }


    @Value
    static class CreateGroup {
        String name;
    }


}
