package com.example.personsrest.domain;


import com.example.personsrest.remote.GroupRemote;
import com.example.personsrest.remote.GroupRemoteImpl;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class PersonService {

    PersonRepository personRepository;
    GroupRemote groupRemote;


    public Stream<Person> all(String search, Integer pagesize, Integer pagenumber) {

        if (search != null && !search.isEmpty()) {
            Pageable pageable = PageRequest.of(pagenumber, pagesize);

            return personRepository.findAllByNameContainingOrCityContaining(search, search, pageable).stream();
        }

        return personRepository.findAll().stream();
    }


    public Person createPerson(String name, int age, String city) {

        PersonEntity personEntity = new PersonEntity(
                UUID.randomUUID().toString(), name, city, age, new ArrayList<>(), true);

        return personRepository.save(personEntity);
    }

    public Person get(String id) {
        return personRepository.findById(id).get();
    }

    public Person updatePerson(String id, String name, int age, String city) {

        Person person = get(id);
        person.setName(name);
        person.setAge(age);
        person.setCity(city);

        return personRepository.save(person);

    }

    public void delete(String id) {
        personRepository.delete(id);
    }


    public Person addGroupToPerson(String id, String name) {

        Person person = get(id);

        person.addGroup(groupRemote.createGroup(name));

        return personRepository.save(person);
    }

    public Person removeGroupFromPerson(String id, String groupName) {
        Person person = get(id);


        String selectedGroupId = person.getGroups().stream()
                .filter(groupId -> groupRemote.getNameById(groupId).equals(groupName))
                .findAny()
                .orElse(null);

        person.removeGroup(selectedGroupId);


        return personRepository.save(person);

    }
}
