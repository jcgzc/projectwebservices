package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api/persons")
@RestController
@AllArgsConstructor
public class PersonController {

    PersonService personService;
    GroupRemote groupRemote;

    @GetMapping
    public List<PersonDTO> all(@RequestParam(value = "search",required = false) String search,
                               @RequestParam(value = "pagesize",required = false) Integer pagesize,
                               @RequestParam(value = "pagenumber",required = false)  Integer pagenumber) {

        return personService.all(search, pagesize , pagenumber).map(this::toDTO).collect(Collectors.toList());

    }

    @PostMapping
    public PersonDTO createPerson(@RequestBody CreatePerson createPerson) {
        return toDTO(personService.createPerson(
                createPerson.getName(),
                createPerson.getAge(),
                createPerson.getCity()
        ));
    }

    @GetMapping("/{id}")
    public PersonDTO get(@PathVariable("id") String id) {
        return toDTO(personService.get(id));
    }

    @PutMapping("/{id}")
    public PersonDTO updatePerson(@PathVariable String id, @RequestBody UpdatePerson updatePerson) {
        return toDTO(personService.updatePerson(
                id,
                updatePerson.getName(),
                updatePerson.getAge(),
                updatePerson.getCity()
        ));
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable("id") String id) {
        personService.delete(id);
    }

    @PutMapping("/{id}/addGroup/{name}")
    public PersonDTO addGroup(@PathVariable("id") String id, @PathVariable("name") String name) {
        return toDTO(personService.addGroupToPerson(id, name));
    }

    @DeleteMapping("/{id}/removeGroup/{name}")
    public PersonDTO removeGroup(@PathVariable("id") String id, @PathVariable("name") String name) {
        return toDTO(personService.removeGroupFromPerson(id, name));
    }


    private PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getAge(),
                person.getCity(),
                true,
                person.getGroups().stream().map(s -> groupRemote.getNameById(s)).collect(Collectors.toList())
        );
    }
}
